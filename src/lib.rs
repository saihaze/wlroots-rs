mod error;
#[macro_use]
mod list;
#[macro_use]
mod listener;
#[macro_use]
mod object;

pub mod allocator;
pub mod backend;
pub mod log;
pub mod output;
pub mod renderer;

#[macro_use]
extern crate wayland_sys;

use wayland_server::Display;
use wayland_sys::server::wl_display_run;

pub fn run_display(display: &Display) {
    unsafe {
        ffi_dispatch!(WAYLAND_SERVER_HANDLE, wl_display_run, display.c_ptr());
    }
}
