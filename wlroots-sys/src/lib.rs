#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

extern crate wayland_sys;

use wayland_sys::common::*;
use wayland_sys::server::*;

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
