use wayland_sys::common::wl_list;

macro_rules! offset_of {
    ($container:ty, $($field:tt)*) => {{
        let tmp = core::mem::MaybeUninit::<$container>::uninit();
        let outer = tmp.as_ptr();
        let inner = core::ptr::addr_of!((*outer).$($field)*) as *const u8;
        inner.offset_from(outer as *const u8)
    }}
}

macro_rules! container_of {
    ($ptr:expr, $container:ty, $($field:tt)*) => {{
        let ptr = $ptr as *const _ as *const u8;
        let offset = offset_of!($container, $($field)*);
        ptr.wrapping_offset(-offset) as *const $container
    }}
}

pub(crate) struct Iter {
    head: *mut wl_list,
    pos: *mut wl_list,
}

impl Iter {
    pub(crate) fn new(head: *mut wl_list) -> Self {
        Self {
            head,
            pos: unsafe { (*head).next },
        }
    }
}

impl Iterator for Iter {
    type Item = *mut wl_list;

    fn next(&mut self) -> Option<*mut wl_list> {
        if self.pos == self.head {
            return None;
        }

        let ptr = self.pos;
        self.pos = unsafe { (*self.pos).next };
        Some(ptr)
    }
}
