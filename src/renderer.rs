use libc::c_void;
use std::pin::Pin;

use wlroots_sys::*;

use crate::backend::Backend;
use crate::error::Error;
use crate::listener;
use crate::object;
use crate::object::{Holder as _, Object};

pub trait Handler: Sized {
    fn destroy(&mut self, _renderer: Renderer<Self>) {}
}

pub(crate) struct Inner<S> {
    base: object::InnerBase<Self>,
    destroy: listener::Listener,
}

impl_listener_set!(Inner, Handler, Listener, destroy);

impl<S> object::Inner for Inner<S> {
    type State = S;
    type Ptr = wlr_renderer;

    fn base(&self) -> &object::InnerBase<Self> {
        &self.base
    }

    fn destroy_ptr(&self) {
        unsafe {
            wlr_renderer_destroy(self.base.ptr());
        }
    }
}

impl<S: Handler> Listener for Inner<S> {
    fn destroy(&mut self, _: *mut c_void) {
        Object::uninit(self, |state, object| {
            Handler::destroy(state, Renderer(object.clone()));
        });
    }
}

pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Color {
    fn wlr(&self) -> *const libc::c_float {
        [self.r, self.g, self.b, self.a].as_ptr()
    }
}

pub struct Renderer<S>(Object<Inner<S>>);
delegate_object!(Renderer);

impl<S: Handler> Renderer<S> {
    pub fn autocreate(backend: &Backend<S>) -> Result<Self, Error> {
        let ptr = unsafe { wlr_renderer_autocreate(backend.object().ptr()).as_mut() };
        let ptr = ptr.ok_or_else(|| Error::new("wlr_renderer_autocreate failed"))?;
        let state = backend.object().state().clone();
        Ok(Renderer(Object::new(ptr, state, |base| Inner {
            base,
            destroy: Default::default(),
        })))
    }

    pub fn begin(&self, width: u32, height: u32) {
        unsafe { wlr_renderer_begin(self.0.ptr(), width, height) }
    }

    pub fn end(&self) {
        unsafe { wlr_renderer_end(self.0.ptr()) }
    }

    pub fn clear(&self, color: &Color) {
        unsafe { wlr_renderer_clear(self.0.ptr(), color.wlr()) }
    }
}

impl<S> object::Holder<Inner<S>> for Renderer<S> {
    fn object(&self) -> &object::Object<Inner<S>> {
        &self.0
    }
}
