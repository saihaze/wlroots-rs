use std::cell::RefCell;
use std::rc::Rc;

use wayland_server::Display;
use wlroots::allocator::Allocator;
use wlroots::backend::Backend;
use wlroots::output::Output;
use wlroots::renderer::{Color, Renderer};
use wlroots::*;

#[derive(Default)]
struct TinywlServer {
    outputs: Vec<Output<Self>>,
    // TODO: these shouldn't be optional
    renderer: Option<Renderer<Self>>,
    allocator: Option<Allocator<Self>>,
}

impl backend::Handler for TinywlServer {
    fn destroy(&mut self, _: Backend<Self>) {
        println!("backend.destroy");
    }

    fn new_output(&mut self, output: Output<Self>) {
        println!("backend.new_output {}", output.name());
        self.outputs.push(output.clone());

        let renderer = &self.renderer.as_ref().unwrap();
        let allocator = &self.allocator.as_ref().unwrap();
        output.init_render(allocator, renderer).unwrap();

        let mode = output.modes().next();
        if let Some(mode) = mode {
            output.set_mode(&mode);
            output.commit().unwrap();
        }
    }
}

impl output::Handler for TinywlServer {
    fn destroy(&mut self, output: Output<Self>) {
        println!("output.destroy");
        self.outputs.retain(|o| o != &output);
    }

    fn frame(&mut self, output: Output<Self>) {
        let renderer = &self.renderer.as_ref().unwrap();
        let (width, height) = output.buffer_size().unwrap();
        output.attach_render().unwrap();
        renderer.begin(width, height);
        renderer.clear(&Color {
            r: 1.0,
            g: 0.0,
            b: 0.0,
            a: 1.0,
        });
        renderer.end();
        output.commit().unwrap();
    }
}

impl renderer::Handler for TinywlServer {
    fn destroy(&mut self, _: Renderer<Self>) {
        self.renderer = None;
    }
}

impl allocator::Handler for TinywlServer {
    fn destroy(&mut self, _: Allocator<Self>) {
        self.allocator = None;
    }
}

fn main() {
    log::init(log::Importance::Debug);

    let server = Rc::new(RefCell::new(TinywlServer::default()));
    let display = Display::new();
    let backend = Backend::autocreate(&display, server.clone()).unwrap();
    let renderer = Renderer::autocreate(&backend).unwrap();
    let allocator = Allocator::autocreate(&backend, &renderer).unwrap();
    server.borrow_mut().renderer = Some(renderer);
    server.borrow_mut().allocator = Some(allocator);
    backend.start().unwrap();

    run_display(&display);
}
